package com.qaagility.controller;

public class About {
    public String desc() {
        return "This is a custom application developed by our team. All necessary information about the application, including its purpose and features, is provided in the project documentation.";
    }
}

